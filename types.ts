export default interface IOrder {
    id: number;
    packages: IPackage[];
    delivery: IDelivery
}

export interface IPackage {
    length: IMesure;
    width: IMesure;
    height: IMesure;
    weight: IMesure;
    products: IProduct[];
}

export interface IMesure {
    unit: string;
    value: number;
}

export interface IProduct {
    name: string;
    price: IMonetary;
}

export interface IMonetary {
    currency: string;
    value: number;
}

export interface IDelivery {
    storePickingInterval: Interval;
    deliveryInterval: Interval;
    contact: IContact;
    carrier: ICarrier;
}

interface Interval {
    start: string;
    end: string;
}

export interface IContact {
    fullname: string;
    email: string;
    phone: string;
    address: string;
    postalCode: string;
    city: string;
}

interface ICarrier {
    name: string;
}
