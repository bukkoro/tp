import Order from "./Order"
import {IPackage} from "types";
import {IDelivery} from "types";

export default class Adapter extends Order{
    
    constructor(id : number,packages : [IPackage] ,delivery : IDelivery){
        super(id,packages,delivery);
        this.Anonymeve();
    }
    public Anonymeve(): void{
        
        this.delivery.contact =  {
            fullname: '*****',
            email: '*****',
            phone: '*****',
            address: '*****',
            postalCode: '*****',
            city: '*****',
          }
    }
}