import IOrder, { IDelivery, IPackage } from "../types"

export default class Order implements IOrder{
    id: number;
    packages: [IPackage];
    delivery: IDelivery;
    
    constructor(id: number,packages: [IPackage],delivery: IDelivery){
        this.id = id
        this.packages = packages
        this.delivery = delivery
    }
  
}