#2

choix du design pattern : MVC
On utilise ce design pattern car il permet d'obtenir une conception plus efficace et plus claire, notamment grâce à la séparation des données de la Vue et du Contrôleur. De plus le MVC permet une plus grande souplesse pour organiser le développement du site entre différents développeurs (indépendance des données, de l’affichage (webdesign) et des actions)

#3 

SOLID = Single Responsibility Principle / Open-Closed Principle / Liskov Substitution Principle / Interface Segregation Principle / Dependency Inversion Principle.

Ici le principe SOLID qui est respecté et utilisé est le Interface Segregation Pinciple qui consiste  à organiser ou hierarchiser les interfaces et ensuite les implémenter.

#4

Pour obtenir ce résultat, le design pattern qui nous sera utile est l'Adaptateur. L’objectif de l'adaptateur est de faire dialoguer un élément hétérogène de notre système avec des éléments homogènes. Et ce, sans perturber le fonctionnement du reste de l’application. Ici il s'agit d'anonymiser les informations du contact, il va donc adapter un objet à une utilisation qui n'était initialement pas prévue.
