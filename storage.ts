import { orders } from './data/_orders'
import storage from 'node-persist'

export default class Storage{
    type : string
    constructor(param : string){
        this.type = param
        this._initDefaultData()
    }
    private _initDefaultData(){
        storage.init().then(() => {
        storage.setItem(this.type, orders)
        })
      }
    public static getItem(param : string) : any {
        return storage.getItem(param) 
    }
}